require 'sidekiq/web'

Rails.application.routes.draw do
  resources :ous
  get '/privacy', to: 'home#privacy'
  get '/terms', to: 'home#terms'
  # get '/about/:id', to: 'home#about', as: 'portal'
  # get '/about/:id/delete', to: 'portals#delete'
authenticate :user, lambda { |u| u.admin? } do
  mount Sidekiq::Web => '/sidekiq'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  namespace :admin do
    resources :users
    resources :announcements
    resources :notifications
    resources :services
    root to: "users#index"
  end

  namespace :madmin do
    namespace :active_storage do
      resources :blobs
    end
    namespace :active_storage do
      resources :variant_records
    end
    resources :announcements
    resources :notifications
    resources :users
    resources :services
    namespace :active_storage do
      resources :attachments
    end
    root to: "dashboard#show"
  end
end

  get '/portals/export', to: 'portals#export'
  resources :about, only: [:show, :delete]
  resources :portals
  resources :places
  resources :actives
  resources :districts
  resources :formats
  resources :auds
  resources :site_infos
  resources :my_events

  get '/events/export', to: 'events#export'
  resources :events

  resources :calendar, only: [:index]
  resources :notifications, only: [:index]
  resources :announcements, only: [:index]
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  root to: 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

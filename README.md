#Установка данного приложения под Ubuntu 20.04 Focal Fossa
#####1. На удалённой машине необходимо создать пользователя deploy с правами sudo
#####2. Теперь устанавливаем окружение Ruby
###### Добавляем репозиторий Node.js
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

Тут могут выйти куча допинфы. Лучше всё сделать.
###### Добавляем репозиторий Yarn 
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo add-apt-repository ppa:chris-lea/redis-server

###### Обновляем список пакетов с новыми репозиториями
    sudo apt-get update
###### Инсталлируем зависимости и компилируем Ruby с поддержкой Node.js и Yarn
    sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev dirmngr gnupg apt-transport-https ca-certificates redis-server redis-tools nodejs yarn

#####3. Далее будем устанавливать версию Ruby 3.0.1
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    git clone https://github.com/rbenv/rbenv-vars.git ~/.rbenv/plugins/rbenv-vars
    exec $SHELL
    rbenv install 3.0.1
    rbenv global 3.0.1
    ruby -v

###### Должен будет выдать что-то рядом с "ruby 3.0.1". Например, ruby 3.0.1p64 (2021-04-05 revision 0fb782ee38) [x86_64-linux]

#####4. Теперь нужно установить Bundler
###### Устанавливаем последний bundler
    gem install bundler
###### Тестируем, что он установился корректно
    bundle -v
###### Должен написать "Bundler version 2.0"
###### Если же напишет, что bundler не найден, запустите rbenv rehash и выполните снова.

#####5. Установка NGINX & Passenger

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
    sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger focal main > /etc/apt/sources.list.d/passenger.list'
    sudo apt-get update
    sudo apt-get install -y nginx-extras libnginx-mod-http-passenger
    if [ ! -f /etc/nginx/modules-enabled/50-mod-http-passenger.conf ]; then sudo ln -s /usr/share/nginx/modules-available/mod-http-passenger.load /etc/nginx/modules-enabled/50-mod-http-passenger.conf ; fi
    sudo ls /etc/nginx/conf.d/mod-http-passenger.conf

Когда NGINX и Passenger установлены, остаётся только настроить Passenger на правильную версию Ruby.

Для этого придётся отредактировать config файл Passenger в редакторе, например, vim:

    sudo vim /etc/nginx/conf.d/mod-http-passenger.conf

Здесь во второй строке заменяем путь на:

    passenger_ruby /home/deploy/.rbenv/shims/ruby;

И можем перезапустить nginx. Если не запустится, то это не страшно. Ещё не всё настроили для этого.
Скорее всего, если посмотреть journalctl -xe, то напишет "Couldn't open /etc/securetty".
Поможет:

    sudo cp /usr/share/doc/util-linux/examples/securetty /etc/securetty
    или
    ln -s /snap/core18/current/etc/securetty /etc/securetty

Но всё-таки нужна настройка под сервис:

    sudo rm /etc/nginx/sites-enabled/default
    sudo vim /etc/nginx/sites-enabled/events

И заполняем следующими данными:

    server {
      listen 80;
      listen [::]:80;
    
      server_name events.edurevda.ru;
      root /home/deploy/events/current/public;
    
      passenger_enabled on;
      passenger_app_env production;
    
      location /cable {
        passenger_app_group_name events_websocket;
        passenger_force_max_concurrent_requests_per_process 0;
      }
    
      # Позволяем файлы к аплоаду до 100MB 
      client_max_body_size 100m;
    
      location ~ ^/(assets|packs) {
        expires max;
        gzip_static on;
      }
    }

###### Даже если на текущий момент nginx не запустится по sudo service nginx reload, то это, скорее всего, потому, что ищет нужные папки, которых пока нет.

#####6. Создаём базу данных PostgreSQL
###### База данных называется events, а должна быть доступна из под пользоватея deploy
    sudo apt-get install postgresql postgresql-contrib libpq-dev
    sudo su - postgres
    createuser --pwprompt deploy
    createdb -O deploy events
    exit

Можно вручную подключиться к базе данных psql -U deploy -W -h 127.0.0.1 -d events. И надо проветить, что именно 127.0.0.1 работает, а не localhost.

# Как наблюдать за работой
###### Так можно посмотреть логи Rails

    less /home/deploy/events/current/log/production.log

###### А так посмотреть логи NGINX и Passenger

    sudo less /var/log/nginx/error.log

###### В принципе, все ошибки будут тут отображаться.

# Создание суперадмина
После регистрации первого пользователя (суперадмина), необходимо на виртуалке из папки с текущей версией приложения, следует:
    
    rails c

И в открывшемся приложении работы с базой данных рельсов написать:

    User.last.update admin:true

Пользователю нужно будет выйти и заново зайти. Он будет суперадминистратором.
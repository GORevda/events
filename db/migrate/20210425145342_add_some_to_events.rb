class AddSomeToEvents < ActiveRecord::Migration[6.1]
  def change
    drop_table :events
    create_table :events do |t|
      t.string :name
      t.string :description
      t.belongs_to :place, column: :id
      t.belongs_to :active, column: :id
      t.belongs_to :district, column: :id
      t.belongs_to :format, column: :id
      t.belongs_to :aud, column: :id
      t.belongs_to :portals, column: :id
      t.date :date
      t.time :start_time
      t.time :duration
      t.integer :max_participants
      t.timestamps
    end

    create_table :events_places, id: false do |t|
      t.belongs_to :event
      t.belongs_to :place
    end

    create_table :actives_events, id: false do |t|
      t.belongs_to :event
      t.belongs_to :active
    end

    create_table :districts_events, id: false do |t|
      t.belongs_to :event
      t.belongs_to :district
    end

    create_table :events_formats, id: false do |t|
      t.belongs_to :event
      t.belongs_to :format
    end

    create_table :events_portals, id: false do |t|
      t.belongs_to :event
      t.belongs_to :portal
    end

    create_table :auds_events, id: false do |t|
      t.belongs_to :event
      t.belongs_to :aud
    end

  end
end

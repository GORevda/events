class CreateAuditorias < ActiveRecord::Migration[6.1]
  def change
    create_table :auditorias do |t|
      t.string :name
      t.timestamps
    end
  end
end

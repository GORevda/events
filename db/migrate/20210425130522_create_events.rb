class CreateEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.belongs_to :place, column: :id
      t.belongs_to :active, column: :id
      t.belongs_to :district, column: :id
      t.belongs_to :format, column: :id
      t.belongs_to :auditoria, column: :id
      t.date :date
      t.time :start_time
      t.time :duration
      t.integer :max_participants
      t.timestamps
    end
  end
end
class CreateMyEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :my_events do |t|
      t.belongs_to :user, column: :id
      t.belongs_to :event, column: :id
      t.timestamps
    end
  end
end

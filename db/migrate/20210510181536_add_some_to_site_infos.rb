class AddSomeToSiteInfos < ActiveRecord::Migration[6.1]
  def change
    add_column :site_infos, :sn_vk, :string
    add_column :site_infos, :sn_facebook, :string
    add_column :site_infos, :sn_twitter, :string
    add_column :site_infos, :sn_ok, :string
  end
end

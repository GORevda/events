class AddPublisherToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :publisher_role, :boolean
    add_column :users, :ou_id, :integer
  end
end

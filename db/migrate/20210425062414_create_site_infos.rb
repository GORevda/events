class CreateSiteInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :site_infos do |t|
      t.string :site_name
      t.string :dev_site_link
      t.string :copyright_string
      t.string :copyright_string_link1_name
      t.string :copyright_string_link1_url
      t.string :copyright_string_link2_name
      t.string :copyright_string_link2_url
      t.timestamps
    end
  end
end

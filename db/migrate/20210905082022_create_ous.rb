class CreateOus < ActiveRecord::Migration[6.1]
  def change
    create_table :ous do |t|
      t.string :name

      t.timestamps
    end
  end
end

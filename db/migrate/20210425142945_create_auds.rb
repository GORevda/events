class CreateAuds < ActiveRecord::Migration[6.1]
  def change
    drop_table :auditorias
    create_table :auds do |t|
      t.string :name
      t.timestamps
    end
  end
end
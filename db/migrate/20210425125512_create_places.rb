class CreatePlaces < ActiveRecord::Migration[6.1]
  def change
    create_table :places do |t|
      t.string :name
      t.string :description
      t.string :contact_fio
      t.string :contact_address
      t.string :contact_email
      t.string :contact_phone
      t.timestamps
    end
  end
end
class CreatePortals < ActiveRecord::Migration[6.1]
  def change
    create_table :portals do |t|
      t.string :name
      t.string :description
      t.string :about
      t.timestamps
    end
  end
end

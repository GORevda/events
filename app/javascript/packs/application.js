// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require actiontext
//= require bootstrap
//= require filterrific/filterrific-jquery
//= require activestorage
//= require_tree .



require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("local-time").start()
require("trix")
require("@rails/actiontext")

import jquery from 'jquery';
window.$ = window.jquery = jquery;

window.Rails = Rails

import 'bootstrap'

$(document).on("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
})


require("trix")
require("@rails/actiontext")
class District < ApplicationRecord

  include PgSearch::Model

  has_and_belongs_to_many :events

  def self.search(search)
    if search
      where('name || cast(id as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

  # def self.options_for_select
  #   districts = District.arel_table
  #   # order('LOWER(name)').map { |e| [e.name, e.id] }
  #   order(districts[:name].lower).pluck(:name, :id)
  # end

end

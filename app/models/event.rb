class Event < ApplicationRecord
  include PgSearch::Model

  has_and_belongs_to_many :actives
  has_and_belongs_to_many :auds
  has_and_belongs_to_many :districts
  has_and_belongs_to_many :formats
  has_and_belongs_to_many :places
  has_and_belongs_to_many :portals

  has_many :my_events

  has_rich_text :description

  filterrific :default_filter_params => { :sorted_by => 'created_at_desc'
                                        },
              :available_filters => %w[
                sorted_by
                search_query
                with_district_id
                with_created_at_gte
                events_params
              ]

  self.per_page = 10

  scope :search_query, ->(query) {
    return nil  if query.blank?
    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 3
    where(
        terms.map {
          or_clauses = [
              "LOWER(events.name) LIKE ?",
              "LOWER(events.name) LIKE ?",
              "LOWER(events.name) LIKE ?"
          ].join(' OR ')
          "(#{ or_clauses })"
        }.join(' AND '),
        *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

  scope :sorted_by, ->(sort_option) {
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    events = Event.arel_table
    districts = District.arel_table
    actives = Active.arel_table
    case sort_option.to_s
    when /^created_at_/
      order(events[:created_at].send(direction))
    when /^Наименования_/
      # order("LOWER(events.name) #{direction}, LOWER(events.name) #{direction}")
      order(events[:name].lower.send(direction)).order(events[:name].lower.send(direction))
    when /^Активность_/
      # order("LOWER(actives.name) #{ direction }").includes(:actives)
      Event.joins(:active).order(actives[:name].lower.send(direction))
    when /^Округ_/
      # order("LOWER(districts.name) #{ direction }").includes(:district)
      Event.joins(:district).order(districts[:name].lower.send(direction)).order(events[:name].lower.send(direction))
    when /^Формат_/
      # order("LOWER(districts.name) #{ direction }").includes(:district)
      Event.joins(:formats).order(formats[:name].lower.send(direction)).order(events[:name].lower.send(direction))
    when /^Аудитория_/
      # order("LOWER(districts.name) #{ direction }").includes(:district)
      Event.joins(:auds).order(auds[:name].lower.send(direction)).order(events[:name].lower.send(direction))
    when /^Дата_/
      order("LOWER(events.date) #{ direction }")
    when /^Начало_/
      order("LOWER(events.start_time) #{ direction }")
    when /^Продолжительность_/
      order("LOWER(events.duration) #{ direction }")
    when /^Участников/
      order("LOWER(events.max_participants) #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :events_params, -> (events_params_attr) {
    events = Event.all
    formats_ids = Format.all.map(&:id).join(', ')
    auds_ids = Aud.all.map(&:id).join(', ')
    actives_ids = Active.all.map(&:id).join(', ')
    places_ids = Place.all.map(&:id).join(', ')
    districts_ids = District.all.map(&:id).join(', ')
    portals_ids = Portal.all.map(&:id).join(', ')
    if !events_params_attr.portals.nil? and events_params_attr.portals != ""
      portals_ids = events_params_attr.portals.join(", ")
    end
    if !events_params_attr.places.nil? and events_params_attr.places != ''
      places_ids = events_params_attr.places.join(", ")
    end
    if !events_params_attr.auds.nil? and events_params_attr.auds != ''
      auds_ids = events_params_attr.auds.join(", ")
    end
    if !events_params_attr.actives.nil? and events_params_attr.actives != ''
      actives_ids = events_params_attr.actives.join(", ")
    end
    if !events_params_attr.districts.nil? and events_params_attr.districts != ''
      districts_ids = events_params_attr.districts.join(", ")
    end

    events = events.joins("left join events_portals ep on ep.event_id = events.id")
                 .joins("left join events_places epl on epl.event_id = events.id")
                 .joins("left join events_formats ef on ef.event_id = events.id")
                 .joins("left join auds_events ea on ea.event_id = events.id")
                 .joins("left join actives_events eac on eac.event_id = events.id")
                 .joins("left join districts_events ed on ed.event_id = events.id")
                 .where("ep.portal_id is NULL or ep.portal_id in (#{portals_ids})")
                 .where("epl.place_id is NULL or epl.place_id in (#{places_ids})")
                 .where("ef.format_id is NULL or ef.format_id in (#{formats_ids})")
                 .where("ea.aud_id is NULL or ea.aud_id in (#{auds_ids})")
                 .where("eac.active_id is NULL or eac.active_id in (#{actives_ids})")
                 .where("ed.district_id is NULL or ed.district_id in (#{districts_ids})")
                .distinct

    if !events_params_attr.with_created_at_gte.nil? and events_params_attr.with_created_at_gte != ''
      events = events.where('events.date >= ?', events_params_attr.with_created_at_gte).distinct
    end

    return events
  }


  def can_registered
    if !self.max_participants.nil?
      return self.my_events.count < self.max_participants
    else
      return true
    end
  end


  def registered_string
    registered = self.my_events.count
    "#{registered} из #{self.max_participants}"
  end

  def time_diff
    seconds_diff = (self.duration - self.start_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}"
    # or, as hagello suggested in the comments:
    # '%02d:%02d:%02d' % [hours, minutes, seconds]
  end

  delegate :name, :to => :district, :prefix => true

  def self.options_for_sorted_by
    [
        ['По названию (а-я)', 'Наименование_asc'],
        ['По названию (я-а)', 'Наименование_desc'],
        ['По дате появления (сначала новые)', 'created_at_desc'],
        ['По дате появления (сначала старые)', 'created_at_asc'],
        ['По округам (а-я)', 'Округ_asc'],
        ['По округам (я-а)', 'Округ_desc'],
        ['По активностям (а-я)', 'Активность_asc'],
        ['По активностям (я-а)', 'Активность_desc']
    ]
  end

  def full_name
    [name, name].compact.join(', ')
  end

  def decorated_created_at
    created_at.to_date.to_s(:long)
  end

  def date_to_formatted_string
    if !self.date.nil?
      string = self.date.strftime("%a %d %b")
    else
      string = "--.--.--"
    end
    return string
  end

  def start_time_to_formatted_string
    if !self.date.nil? and !self.start_time.nil?
      string = self.date.strftime("%d.%m").to_s
      string += ' ' + self.start_time.strftime("%H:%M").to_s
    else
      string = "--.--.-- --.--"
    end
    return string
  end

  def end_time_to_formatted_string
    if !self.duration.nil?
      string = self.duration.strftime("%H:%M")
    else
      string = "--:--"
    end
    return string
  end

  def duration_formatted_string
    if !self.date.nil?
      string = self.date.strftime("%d.%m.%Y").to_s
    else
      string = '--.--.--'
    end
    string += ' '

    if !self.start_time.nil?
      string += self.start_time.strftime("%H:%M").to_s
    else
      string += '--:--'
    end
    string += '-'

    if !self.start_time.nil?
      string += self.duration.strftime("%H:%M").to_s
    else
      string += '--:--'
    end

    return string
  end


end

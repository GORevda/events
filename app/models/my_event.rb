class MyEvent < ApplicationRecord
  include PgSearch::Model

  belongs_to :user
  belongs_to :event

  self.per_page = 10

  def self.search(search)
    if search
      MyEvent.joins("join events on events.id = my_events.event_id").where('events.name LIKE ?', "%#{search}%").all
    end
  end

end

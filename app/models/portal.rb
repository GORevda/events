class Portal < ApplicationRecord

  include PgSearch::Model

  self.per_page = 20

  WillPaginate.per_page = 20

  has_one_attached :logo_image
  has_and_belongs_to_many :events

  attr_accessor :delete_logo_image

    def self.search(search)
    if search
      where('name || cast(id as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

end

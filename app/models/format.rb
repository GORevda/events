class Format < ApplicationRecord

  include PgSearch::Model

  has_and_belongs_to_many :event

  def self.search(search)
    if search
      where('name || cast(id as varchar(5)) LIKE ?', "%#{search}%")
    end
  end

end

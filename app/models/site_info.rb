class SiteInfo < ApplicationRecord


  self.per_page = 20

  WillPaginate.per_page = 20

  has_one_attached :logo_image
  attr_accessor :delete_logo_image

  has_one_attached :dev_logo_image
  attr_accessor :delete_dev_logo_image


end

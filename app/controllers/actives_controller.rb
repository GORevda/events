class ActivesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @actives = Active.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @actives = Active.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @actives = Active.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @active = Active.new
    if !@active.nil?
      render :edit
    else
      flash[:success] = t("actives.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @actives = Active.accessible_by(current_ability)
    render xlsx: 'actives_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/actives/export.xlsx.axlsx'
  end

  def show
    @active = Active.accessible_by(current_ability).find(params[:id])
  end

  def create
    @active = Active.new(active_params)
    if @active.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @active = Active.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Active.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @active = Active.accessible_by(current_ability).find(params[:id])
    @active.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("actives.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @active = Active.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @active.update active_params
      flash[:success] = t("actives.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def active_params
    params.require(:active).permit(:name)
  end

end

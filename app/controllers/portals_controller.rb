class PortalsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @portals = Portal.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @portals = Portal.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @portals = Portal.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @portal = Portal.new
    if !@portal.nil?
      render :edit
    else
      flash[:success] = t("portals.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @portals = Portal.accessible_by(current_ability)
    render xlsx: 'portals_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/portals/export.xlsx.axlsx'
  end

  def show
    @portal = Portal.accessible_by(current_ability).find(params[:id])
  end

  def create
    @portal = Portal.new(portal_params)
    if @portal.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @portal = Portal.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Portal.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @portal = Portal.accessible_by(current_ability).find(params[:id])
    @portal.destroy
    respond_to do |format|
      format.html { redirect_to portals_url, notice: t("portals.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @portal = Portal.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @portal.update portal_params
      flash[:success] = t("portals.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  # def update
  #   @portal = Portal.find(params[:id])
  #   if @portal.update(portal_params)
  #     redirect_to root_path, alert: t("portals.notice.updated")
  #   else
  #     render 'edit', alert: "Oops! There was a problem, please try again"
  #   end
  # end


  def portal_params
    params.require(:portal).permit(:name, :description, :about, :logo_image, :delete_logo_image)
  end

end


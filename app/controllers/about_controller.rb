class AboutController < ApplicationController
  def index
  end

  def terms
  end

  def privacy
  end

  def show
    @portal = Portal.find(params[:id])
  end

end

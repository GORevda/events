class SiteInfosController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    new
  end

  def new
    @site_info = SiteInfo.all.first
    if !@site_info.nil?
      render :edit
    else
      @site_info = SiteInfo.new
      if !@site_info.nil?
        render :edit
      else
        flash[:success] = t("site_infos.errors.cant_create_new")
        redirect_to root_path
      end
    end
  end


  def show
    @new
  end

  def create
    @site_info = SiteInfo.new(site_info_params)
    if @site_info.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
  end

  def delete
    SiteInfo.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @site_info = SiteInfo.accessible_by(current_ability).find(params[:id])
    @site_info.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("site_infos.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @site_info = SiteInfo.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @site_info.update site_info_params
      flash[:success] = t("site_infos.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def site_info_params
    params.require(:site_info).permit(:site_name, :dev_site_link, :logo_image,
                                      :delete_logo_image, :dev_logo_image, :delete_dev_logo_image,
                                      :copyright_string, :copyright_string_link1_name, :copyright_string_link1_url,
                                      :copyright_string_link2_name, :copyright_string_link2_url,
                                      :sn_vk, :sn_facebook, :sn_twitter, :sn_ok)
  end

end

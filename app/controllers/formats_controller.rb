class FormatsController < ApplicationController

  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @formats = Format.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @formats = Format.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @formats = Format.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @format = Format.new
    if !@format.nil?
      render :edit
    else
      flash[:success] = t("formats.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @formats = Format.accessible_by(current_ability)
    render xlsx: 'formats_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/formats/export.xlsx.axlsx'
  end

  def show
    @format = Format.accessible_by(current_ability).find(params[:id])
  end

  def create
    @format = Format.new(format_params)
    if @format.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @format = Format.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Format.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @format = Format.accessible_by(current_ability).find(params[:id])
    @format.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("formats.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @format = Format.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @format.update format_params
      flash[:success] = t("formats.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def format_params
    params.require(:format).permit(:name)
  end

end

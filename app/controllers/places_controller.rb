class PlacesController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @places = Place.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @places = Place.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @places = Place.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @place = Place.new
    if !@place.nil?
      render :edit
    else
      flash[:success] = t("places.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @places = Place.accessible_by(current_ability)
    render xlsx: 'places_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/places/export.xlsx.axlsx'
  end

  def show
    @place = Place.accessible_by(current_ability).find(params[:id])
  end

  def create
    @place = Place.new(place_params)
    if @place.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @place = Place.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Place.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @place = Place.accessible_by(current_ability).find(params[:id])
    @place.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("places.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @place = Place.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @place.update place_params
      flash[:success] = t("places.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def place_params
    params.require(:place).permit(:name, :description, :contact_fio, :contact_address, :contact_email, :contact_phone)
  end

end

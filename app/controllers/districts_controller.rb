class DistrictsController < ApplicationController

  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @districts = District.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @districts = District.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @districts = District.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @district = District.new
    if !@district.nil?
      render :edit
    else
      flash[:success] = t("districts.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @districts = District.accessible_by(current_ability)
    render xlsx: 'districts_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/districts/export.xlsx.axlsx'
  end

  def show
    @district = District.accessible_by(current_ability).find(params[:id])
  end

  def create
    @district = District.new(district_params)
    if @district.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @district = District.accessible_by(current_ability).find(params[:id])
  end

  def delete
    District.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @district = District.accessible_by(current_ability).find(params[:id])
    @district.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("districts.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @district = District.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @district.update district_params
      flash[:success] = t("districts.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def district_params
    params.require(:district).permit(:name)
  end

end

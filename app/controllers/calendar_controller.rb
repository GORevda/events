class CalendarController < ApplicationController

  def index

    (@filterrific = initialize_filterrific(
        Event,
        params[:filterrific],
        select_options: {
            sorted_by: Event.options_for_sorted_by,
            # district_id: District.options_for_select,
        },
        )) || return
    @events = @filterrific.find.page(params[:page])
    @districts = District.all
    @auds = Aud.all
    @portals = Portal.all
    @places = Place.all
    @actives = Active.all
    respond_to do |format|
      format.html
      format.js
    end
  rescue ActiveRecord::RecordNotFound => e
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end

end

class AudsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!

  def index
    @auds = Aud.accessible_by(current_ability).paginate(page: params[:page]).order('id ASC')
    if current_user.admin?
      if !params[:search].nil?
        @auds = Aud.all.search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    else
      if !params[:search].nil?
        @auds = Aud.accessible_by(current_ability).search(params[:search]).paginate(page: params[:page]).order('id ASC')
      end
    end
  end

  def new
    @aud = Aud.new
    if !@aud.nil?
      render :edit
    else
      flash[:success] = t("auds.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    @auds = Aud.accessible_by(current_ability)
    render xlsx: 'auds_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/auds/export.xlsx.axlsx'
  end

  def show
    @aud = Aud.accessible_by(current_ability).find(params[:id])
  end

  def create
    @aud = Aud.new(aud_params)
    if @aud.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @aud = Aud.accessible_by(current_ability).find(params[:id])
  end

  def delete
    Aud.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @aud = Aud.accessible_by(current_ability).find(params[:id])
    @aud.destroy
    respond_to do |format|
      format.html { redirect_to ates_url, notice: t("auds.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @aud = Aud.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @aud.update aud_params
      flash[:success] = t("auds.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def aud_params
    params.require(:aud).permit(:name)
  end

end

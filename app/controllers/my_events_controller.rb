class MyEventsController < ApplicationController
  load_and_authorize_resource :instance_name => :item
  before_action :authenticate_user!
  before_action :fill_directories

  def index
    @my_events = MyEvent.accessible_by(current_ability).paginate(page: params[:page]).order('id DESC')

    if params[:search]
      @my_events = @my_events.search(params[:search]).order("created_at DESC")
    else
      @my_events = @my_events.all.order('created_at DESC')
    end

  end

  def new
    @my_event = MyEvent.new
    if !@my_event.nil?
      render :edit
    else
      flash[:error] = t("my_events.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def show
    @my_event = MyEvent.accessible_by(current_ability).find(params[:id])
  end

  def create
    @my_event = MyEvent.new(my_event_params)
    if !@my_event.event.nil?
      if @my_event.event.can_registered
        if @my_event.save
          redirect_to :action => 'index'
        else
          render :action => 'new'
        end
      else
        flash[:error] = t("my_events.errors.cant_create_new_max_participants")
        render :action => 'new'
      end
    end
  end

  def edit
    @my_event = MyEvent.accessible_by(current_ability).find(params[:id])
  end

  def delete
    MyEvent.accessible_by(current_ability).find(params[:id]).destroy
    return
  end

  def destroy
    @my_event = MyEvent.accessible_by(current_ability).find(params[:id])
    @my_event.destroy
    respond_to do |format|
      format.html { redirect_to my_events_url, notice: t("my_events.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    @my_event = MyEvent.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @my_event.update my_event_params
      flash[:success] = t("my_events.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def my_event_params
    params.require(:my_event).permit(:event_id, :user_id)
  end

  def fill_directories
    @events = Event.all.where("events.date >= now() or events.date is NULL")
  end

end

class EventsController < ApplicationController

  load_and_authorize_resource :instance_name => :item

  def index

    (@filterrific = initialize_filterrific(
        Event,
        params[:filterrific],
        select_options: {
            sorted_by: Event.options_for_sorted_by,
            # district_id: District.options_for_select,
        },
        )) || return
    @events = @filterrific.find.page(params[:page])
    @districts = District.all
    @auds = Aud.all
    @portals = Portal.all
    @places = Place.all
    @actives = Active.all
    respond_to do |format|
      format.html
      format.js
    end
    rescue ActiveRecord::RecordNotFound => e
      puts "Had to reset filterrific params: #{ e.message }"
      redirect_to(reset_filterrific_url(format: :html)) and return
  end

  def new
    # before_action :authenticate_user!
    @event = Event.new
    if !@event.nil?
      render :edit
    else
      flash[:success] = t("events.errors.cant_create_new")
      redirect_to :action => 'index'
    end
  end

  def export
    # before_action :authenticate_user!
    @events = Event.accessible_by(current_ability)
    render xlsx: 'events_list_' + DateTime.now.getlocal.to_s.gsub(' ', '-').gsub(':', '.'), template: '/events/export.xlsx.axlsx'
  end

  def show
    @event = Event.accessible_by(current_ability).find(params[:id])
  end

  def create
    # before_action :authenticate_user!
    @event = Event.new(event_params)
    if @event.save
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    # before_action :authenticate_user!
    @event = Event.accessible_by(current_ability).find(params[:id])
  end

  def delete
    # before_action :authenticate_user!
    Event.accessible_by(current_ability).find(params[:id]).destroy
  end

  def destroy
    # before_action :authenticate_user!
    @event = Event.accessible_by(current_ability).find(params[:id])
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: t("events.notice.destroyed") }
      format.json { head :no_content }
    end
    # redirect :action => 'index'
  end

  def update
    # before_action :authenticate_user!
    @event = Event.accessible_by(current_ability).find params[:id]
    # respond_to do |format|
    if @event.update event_params
      flash[:success] = t("events.notice.updated")
      redirect_to :action => 'index'
    else
      render :action => 'edit'
    end
  end

  def event_params
    params.require(:event).permit(:name, :description, :date, :start_time, :duration, :max_participants, :created_at, :updated_at, :portals, place_ids:[], active_ids:[], district_ids:[], format_ids:[], aud_ids:[], portal_ids:[])
  end

end

# category_ids:[], categories_attributes: [:name]
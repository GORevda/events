class HomeController < ApplicationController
  def index
  end

  def terms
  end

  def privacy
  end

  def about
    @portal = Portal.find(params[:id])
  end


end
